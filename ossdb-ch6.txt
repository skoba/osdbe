/* p.43 */
Chapter 6. Applications of Database Definitions

A good definition of a database will raise its integrity and efficiency.  In this Chapter explained are the fundamental knowledges for database definitions such as Primary Key and Foreign Key, and useful functions such as Sequence.

6.1 Primary Key

A primary key is a column or a group of columns that can identify the row data uniquely.  UNIQUE means no duplication within the values of the column.  A unique column is also called a Unique Key.  A primary key must be not only a unique key but also having values in each field of the column, which is called "NOT NULL".  NULL will be discussed later in this chapter.
As the following example shows, if you use the selecting condition on the column order_id from the orders table, you can pick up only one row, on the other hand, if you use the condition on the column customer_id, more than one rows will be selected.

[.fig]

The column order_id can be used as the primary key since it has unique values, while the column customer_id cannot be used as the primary key because its values are not unique.

6.1.1 Specifying Primary Keys

You can specify the primary key using CREATE TABLE statement when creating a table, or using ALTER TABLE statement to the existing table.

ALTER TABLE command syntax that specify the primary key
/* p.44 */
[ ALTER TABLE tablename ADD PRIMARY KEY(columnname[,...])  ]

In the following example, the column prod_id from the prod table is specified as the primary key.  If you specify a primary key, the index will be automatically generated (without notification) which accelerates the search.  The column specified as the primary key will be imposed NOT NULL constraint, and the unique index with the name "columnname_pkey" will be generated.

[.fig]

Let us specify the column order_id from the orders table and the column customer_id from the customer table as primary keys.

[.fig]

6.1.2 Checking the Primary Key Behavior

If specified as the primary key, "UNIQUE" and "NOT NULL" constraints will be imposed, which makes it impossible some operations such as inserting some kind of row data.
As the following examples show, an INSERT statement with not specifying value (becomes NULL) or specifying already existing value to the column prod_id from the prod table (primary key) will make errors.

[.fig]
/* p.45 */
[.fig]

6.1.3 Primary Key comprised of multiple columns

As we defined the primary key as "a column or a group of columns that can identify the row data uniquely", it is possible to define a primary key comprised of multiple columns, which is called a multicolumn primary key or multicolumn key.
A good example for multicolumn key would be "1st Grade, Class #2, Student Serial No.3", which is a composite of multiple elements.


6.2 Foreign Key

A foreign key is defined as a column whose values match the values appearing in the primary key (or unique key - this applies hereafter) of another table.  It is called the "foreign key reference" that a foreign key references the primary key of another table, and the referenced primary key is called "referenced key".

6.2.1 Referential Integrity Constraint

In case of the foreign key reference, "referential integrity constraint" (also called "foreign key constraint") assures the existence of the values in the referenced key.  The foreign key constraint makes it an error if inserting a value which does not exist in the referenced key to the foreign key or updating the forein key value to a value which does not exist in the referenced key, so it prevents the foreign key values from having wrong values.  It also prevents the values referenced from a foreign key from deleting from the referenced key, which secures the availability of the values used in the foreign key of another table.

6.2.2 Specifying Foreign Keys

You can specify the foreign key using CREATE TABLE statement when creating the table, or using ALTER TABLE statement to the existing table.

ALTER TABLE command syntax that specify the foreign key
/* p.44 */
[ ALTER TABLE tablename ADD FOREIGN KEY(columnname) REFERENCES referencedtable(referencedkeyname)  ]

In the following example, foreign keys are specified to the columns customer_id and prod_id from the orders table, referencing the customer and prod tables, respectively.

/* p.46 */
[.fig]

In the following example, because the column customer_id from the customer table has no row value of 4, inserting it violates the foreign key constraint and makes an error.  Similarly, because the column prod_id from the prod table has no row value of 6, inserting it violates the foreign key constraint and makes an error.

[.fig]
/* p.47 */
[.fig]

[.table
                        Foreign key reference
                           +------------+
                           |            v
           +----------+---------+  +---------+-----------+-------+
           | order_id | prod_id |  | prod_id | prod_name | price |
           +----------+---------+  +---------+-----------+-------+
           |        1 |       1 |  |       1 |   *****   |    50 |
           |          |         |  |         |           |       |
           |        2 |       2 |  |       2 |   *****   |    70 |
           |          |         |  |         |           |       |
           |        3 |       3 |  |       3 |   *****   |   100 |
           |          |         |  |         |           |       |
           |        4 |       1 |  |       4 |   *****   |    31 |
           |          |         |  |         |           |       |
           |        5 |       2 |  |       5 |   *****   |    60 |
           |          |         |  +---------+-----------+-------+
(1)UNSERT->|        6 |       6 |       A                         
           +----------+---------+       |(2)Reference integrity check
                           |            |
                           +------------+
           Because there is no row data with the prod_id value of six, INSERT makes an error.
]


6.2.3 Specifying Primary Key and Foreign Key using CREATE TABLE statement

Primary keys and foreign keys can be specified by the CREATE TABLE statement.
The following examples show CREATE TABLE statements that specify primary keys and foreign keys to the tables prod, customer and orders.  Since specifying foreign keys requires the primary key of another table to be the referenced key, the referenced table should have been created beforehand.

[.fig]
/* p.48 */
[.fig]

6.2.4 Are Primary Keys and Foreign Keys necessary?

By specifying primary keys and foreign keys, it can be avoided to insert unnecessary duplication to the table or erroneously delete a row data.  On the other hand, since it will not be allowed temporary inconsistency status, you may have some inconvenience when maintaining the data.  To avoid the inconvenience, sometimes consistency is checked not by database constraints but by application side.
Though it depends on cases, fundamentally, primary keys and foreign keys shoud be set at the database side, and if it causes problems in operation, it could be removed.  At least from database design point, the idea of primary keys and foreign keys are important.  As well, normalizing (mentioned later) is also important in database design.

6.2.5 Normalization

Normalization is the database design methodology that defines how to store data into a relational database.  Since a relational database stores data as tables, it will be a job to split actual row data into tables so that the data can be stored into tables nicely.
There are several normalization formats such as the First Normal Form (1NF), the Second Normal Form (2NF) and the Third Normal Form (3NF).  In short, the more normalized the database, the more splitted the tables and increase the number of them.  The tables getting simpler by splitting, the possibility of making problems while updating, deleting or adding row data becomes lower.
Though this book will not cover the normalization in detail, it is a must-know item in designing databases, so you are encouraged to study it using reference books, etc.


6.3 NULL

NULL is defined as "unknown" or "undefined", and distinguished from "zero", "blank" or "empty character".  Numerical zero or empty character is a status of data "exists", however, NULL is the status of data "does not exist".

6.3.1 NOT NULL Constraint

If NULL is not allowed in a column, NOT NULL constraint should be set in the table definition.  Values are always required in the column where NOT NULL constraint is imposed.  Since a primary key always requires the values, primary key definition automatically sets the NOT NULL constraint.
In the following example, because the column customer_id from the customer table is specified as a primary key, NOT NULL constraint is imposed.

/* p.49 */
[.fig]

6.3.2 Detection of NULL

Since NULL does not have a value, it cannot be searched by conditional expressions with usual operators.  To detect if a column includes NULL, IS NULL or IS NOT NULL operators may be used.

[.fig]

6.3.3 Handling of NULL in Aggregate Functions

Aggregate functions may or may not ignore NULL.  For example, count(*) counts NULL as well, however, avg function ignores it when calculating the arithmetic mean.  Aggregate functions such as sum, max and min also ignore NULL.
In the following example, the value of sum(price) is 311 and count(*) is 6, however, because price table has a NULL in it, avg(price) returns 311 �� 5 = 62.2 as the result.

/* p.50 */
[.fig]

6.3.4 Empty Character

Similar to NULL is an "empty character".  An empty character can be specified by "''" (two contiguous single quotes).  An empty character is not NULL, so it does not violate NOT NULL constraint.
In the following example, an empty character is inserted to the column prod_name from the prod table.  It will not be detected by IS NULL operator.

[.fig]

6.4 Sequence

A sequence generates sequence numbers.  For example, if you use a sequence in the INSERT statement, sequence numbers will be inserted as the values, it is suitable for unique value generation such as ID numbers.

6.4.1 Creation of Sequences

A sequence is created by CREATE SEQUENCE command.
In the following example, the sequence order_id_seq is created with parameters left as their defaults.  Default values of a sequence.

[.fig]
/* p.51 */
Table 6.1 Default values of a sequence.
+-------------+---------------------------------------------------------+
|Initial value|1                                                        |
|             |                                                         |
|Increment    |1                                                        |
|             |                                                         |
|Maximum value|2 raised to the 63rd power -1 (9,223,372,036,854,775,807)|
+-------------+---------------------------------------------------------+

6.4.2 Sequence Manipulations

currval('sequence')
returns the current value of the sequence.  Sequence value is not modified.

nextval('sequence')
returns the next value of the sequence, and modifies the sequence value to the next one.  By default, as sequence value is incremented by one, if the current value is 1, then the next value will be 2.  After reaching the maximum value, nextval call makes an error.

setval('sequence', value)
sets the sequence value to the specified value.

In the following example, values are pulled from the sequence order_id_seq.

[.fig]
/* p.52 */
The sequence value can be reset by the function setval().  Values between 1 and the default maximum value can be specified.

[.fig]

6.4.3 Using Sequence in SQL statements

A sequence is used in e.g. INSERT statement.
In the following example, in the INSERT statement inserting row data to the orders table, the values of the column order_id will be obtained from the sequence order_id_seq.

[.fig]

/* p.53 */
6.4.4 Sequence and Skipped Numbers

Sequence numbers can be easily obtained from a sequence, however, the value of the sequence increments even if the SQL statement fails.  In such a case, a "skipped number" occurs.  A sequence does not assure complete sequence numbers, or it would be difficult to generate complete sequence numbers.  For example, deleting in-between rows makes the sequence numbers incomplete.  So, taking the sequence numbers just as unique identifiers that distinguish row data makes the system design easy.
In the following example, even if INSERT statement fails, the sequence value still increments and a skipped number occurs at the next INSERT statement.

/* End of Chapter 6 */
